(function($){
	// menu
	// $(".menu-bar").click(function (e) {
	// 	$(this).toggleClass("on");
	// 	$("#main-header .navGlobalIn").toggleClass("show-menu");
	// 	$("html").toggleClass("none-scroll");
	// });
})(jQuery);

//slick slide
// jQuery(function ($) {
// 	$('.slide-slick').slick({
// 		dots: false,
// 		slidesToShow: 1,
// 		slidesToScroll: 1,
// 		centerPadding: '0px',
// 		speed: 700,
// 		prevArrow:"<i class=\"fa fa-angle-left prev\" aria-hidden=\"true\"></i>",
// 		nextArrow:"<i class=\"fa fa-angle-right next\" aria-hidden=\"true\"></i>",
// 		speed: 700,
// 		autoplay: true,
// 		autoplaySpeed: 3000,
// 		// fade: true,
// 		// cssEase: 'linear',
// 		// lazyLoad: 'ondemand',
// 		lazyLoadBuffer: 0,
// 		pauseOnHover: true,
// 		pauseOnFocus: true,
// 		focusOnSelect: true,
// 	});
// });

//matchHeight
(function($){
	$(".th").matchHeight();
	$(".sec-list-number .flex .item .ttl-item").matchHeight();
})(jQuery);

$(function () { objectFitImages('img'); });
$(document).ready(function(){
	$('.btn-menu').click(function(){
		$(this).find(".menu").toggleClass('open');
		$("body").toggleClass("fix");
		$("#main-header .navGlobal").toggleClass("open");
		$(".btn_type01").toggleClass("hide");
	});
});

$(function(){
	$('.link[href^="#"]').click(function () {
// e.preventDefault();
		var headerHeight = $("#main-header").outerHeight();
		var speed = 800;
		var href = jQuery(this).attr("href");
		var target = jQuery(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop: position}, speed, 'swing');
		return false;
	});
});


$(document).ready(function () {

	$('#H-bar-1').jqbar({ label: '<span>10</span>代', value: 7});

	$('#H-bar-2').jqbar({ label: '<span>20</span>代', value: 49});

	$('#H-bar-3').jqbar({ label: '<span>30</span>代', value: 31});

	$('#H-bar-4').jqbar({ label: '<span>40</span>代', value: 9 });

	$('#H-bar-5').jqbar({ label: '<span>50</span>代', value: 3});

	$('#H-bar-6').jqbar({ label: '<span>60</span>代以降', value: 1 });


	$('#H1-bar-1').jqbar1({ label: '<span>1</span>年未満', value: 25});

	$('#H1-bar-2').jqbar1({ label: '<span>1~5</span>年', value: 30});

	$('#H1-bar-3').jqbar1({ label: '<span>5~10</span>年', value: 20});

	$('#H1-bar-4').jqbar1({ label: '<span>10~15</span>年', value: 16 });

	$('#H1-bar-5').jqbar1({ label: '<span>15~20</span>年', value: 5});

	$('#H1-bar-6').jqbar1({ label: '<span>20</span>年以上', value: 4 });

});